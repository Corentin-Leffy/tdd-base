package dojo

import (
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Example test. Adapt and complete
// Uses ginkgo with gomega framework:
// https://onsi.github.io/gomega/#provided-matchers
var _ = Describe("Hello world", func() {
	It("says hello", func() {
		res := "Hello world"

		Expect(strings.ToLower(res)).To(ContainSubstring("hello"))
	})
})
